%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. трав 2015 20:15
%%%-------------------------------------------------------------------
-module(p02).
-author("Bohdan").

%% API
-export([but_last/1]).


%% --------------------------
%% implementation of task PO2
%% --------------------------
but_last([]) -> "Error: empty list";
but_last([_]) -> "Error: list length < 2";
but_last([H1,H2|[]]) -> [H1,H2];
but_last([_,H2|T]) -> but_last([H2] ++ T).

