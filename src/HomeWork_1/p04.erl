%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. трав 2015 20:48
%%%-------------------------------------------------------------------
-module(p04).
-author("Bohdan").

%% API
-export([len/1,len2/1]).


%% --------------------------
%% implementation of task P04
%% with tail rec
%% --------------------------
len(List) -> len(List, 0).
len([], Count) -> Count;
len([_|T], Count) -> len(T, Count + 1).


%% simple solve
len2([_|T]) -> 1 + len2(T);
len2([]) -> 0.
