%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. трав 2015 11:31
%%%-------------------------------------------------------------------
-module(p08).
-author("Bohdan").

%% API
-export([compress/1]).


%% --------------------------
%% implementation of task p08
%% --------------------------
compress([]) -> [];
compress([H|T]) -> compress(T, [H]).
compress([] , ReversAns) ->
  p05:reverse(ReversAns);
compress([H|T], [H|Tans]) -> compress(T, [H|Tans]);
compress([NewH|T], [H|Tans]) -> compress(T, [NewH,H|Tans]).

