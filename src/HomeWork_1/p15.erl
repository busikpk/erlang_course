%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. трав 2015 14:28
%%%-------------------------------------------------------------------
-module(p15).
-author("Bohdan").

%% API
-export([replicate/2,replicate2/2]).


%% --------------------------
%% implementation of task p15
%% --------------------------
replicate([], _) -> [];
replicate([H|T], Times) ->  dup(H, Times, []) ++ replicate(T, Times).

dup(_, 0, Res) -> Res;
dup(H, Times, Res) -> dup(H, Times - 1, Res ++ [H]).


%% solve with tail rec
replicate2(List, Times) -> replicate2(List, Times, []).
replicate2([], _, Res) -> p05:reverse(Res);
replicate2([H|T], Times, Res) ->
  ListOfDuplicates = dup(H, Times, []),
  replicate2(T, Times, [ListOfDuplicates|Res]).
