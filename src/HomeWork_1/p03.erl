%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. трав 2015 20:38
%%%-------------------------------------------------------------------
-module(p03).
-author("Bohdan").

%% API
-export([element_at/2]).


%% --------------------------
%% implementation of task P03
%% --------------------------
element_at([H|_], 1) -> H;
element_at([], _ ) -> "Error: incorrect index";
element_at([_|T], Index) ->
  element_at(T, Index - 1).
