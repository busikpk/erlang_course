%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. трав 2015 22:01
%%%-------------------------------------------------------------------
-module(p06).
-author("Bohdan").

%% API
-export([is_palindrome/1]).


%% --------------------------
%% implementation of task P06
%% --------------------------
is_palindrome(List) ->
  ReversList = p05:reverse(List),
  ReversList == List.

