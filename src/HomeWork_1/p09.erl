%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. трав 2015 12:12
%%%-------------------------------------------------------------------
-module(p09).
-author("Bohdan").

%% API
-export([pack/1,pack2/1]).

%% -----------------------------
%% implementation of task p09
%% version 1.0
%% -----------------------------
pack([]) -> [];
pack([H|T]) -> pack([H|T],[H]).

pack([], Res) -> Res;
pack([H|T], [H|TRes]) ->
  SubList = packSubList(T,[H]),
  pack([H|T] -- SubList, [TRes ++ SubList]);

pack([H|T],[HRes|TRes]) ->
  SubList = packSubList(T, [H]),
  pack(T -- [H|SubList],[HRes|TRes] ++ [SubList]).

packSubList([], SubList) -> SubList;
packSubList([H|T], [H|T1]) -> packSubList(T, [H,H|T1]);
packSubList([_|_],SubList) -> SubList.


%%---------------------------
%% implementation of task p09
%% without '++' and '--'
%% --------------------------
pack2([])-> [];
pack2([H|T]) -> packer(T, [H], []).

packer([], SubList, Acc) -> p05:reverse([SubList|Acc]);
packer([H|T], [H|T2], Acc) -> packer(T, [H,H|T2], Acc);
packer([H|T], SubList, Acc ) -> packer(T, [H], [SubList|Acc]).
