%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. трав 2015 11:55
%%%-------------------------------------------------------------------
-module(p11).
-author("Bohdan").

%% API
-export([encodemodified/1]).


%% --------------------------
%% implementation of task p11
%% --------------------------
encodemodified([]) -> [];
encodemodified(List) ->
   Target = p09:pack(List),
   lenOfDup(Target, []).

lenOfDup([], Res) -> p05:reverse(Res);
lenOfDup([[H|T1]|T2], Res) ->
  Length = p04:len([H|T1]),
  ItemOfList = itemPresentation(Length, H),
  lenOfDup(T2, [ItemOfList|Res]).

itemPresentation(1, H) -> H;
itemPresentation(Len, H) -> {Len, H}.
