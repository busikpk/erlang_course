%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. трав 2015 11:28
%%%-------------------------------------------------------------------
-module(p07).
-author("Bohdan").

%% API
-export([flatten/1,flatten2/1]).


%% --------------------------
%% implementation of task p07
%% --------------------------
flatten([])->[] ;
flatten([[]|T])-> flatten(T);
flatten([[H|T]|T2])-> flatten([H|[T|T2]]);
flatten([H|T])-> [H|flatten(T)].



%% --------------------------
%% implementation of task p07
%% with tail rec.
%% --------------------------
flatten2([]) -> [];
flatten2(List) -> flatten_tail_rec(List, []).
flatten_tail_rec([], Res) -> p05:reverse(Res);
flatten_tail_rec([[]|T], Res) -> flatten_tail_rec(T, Res);
flatten_tail_rec([[H|T]|T2], Res) -> flatten_tail_rec([H|[T|T2]], Res);
flatten_tail_rec([H|T], Res) -> flatten_tail_rec(T, [H|Res]).