%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. трав 2015 12:29
%%%-------------------------------------------------------------------
-module(p13).
-author("Bohdan").

%% API
-export([decode/1]).


%% -------------------------
%% implementation of task p13
%% -------------------------
decode([]) -> [];
decode(List) -> decoder(List, []).

decoder([], Ans) -> p05:reverse(Ans);
decoder([H|T], Ans) ->
  ItemOfList = itemReg(H),
  decoder(T, [ItemOfList|Ans]).

itemReg({}) -> [{}];
itemReg({Length, Value}) -> genDup(Value, Length, []).

genDup(_, 0, Ans) -> Ans;
genDup(Value, Times, Ans) ->
  genDup(Value, Times - 1, [Value|Ans]).