%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. трав 2015 12:08
%%%-------------------------------------------------------------------
-module(p12).
-author("Bohdan").

%% API
-export([decodemodified/1]).


%% --------------------------
%% implementation of task p12
%% --------------------------
decodemodified([]) -> [];
decodemodified(List) -> decoder(List, []).

decoder([], Ans) -> p05:reverse(Ans);
decoder([H|T], Ans) ->
  ItemOfList = itemReg(H),
  decoder(T, [ItemOfList|Ans] ).

itemReg({Length, Value}) -> genDup(Value, Length, []);
itemReg(H) -> [H].

genDup(_, 0, Ans) -> Ans;
genDup(Value, Times, Ans) ->
  genDup(Value, Times - 1, [Value|Ans]).
