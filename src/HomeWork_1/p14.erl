%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. трав 2015 14:18
%%%-------------------------------------------------------------------
-module(p14).
-author("Bohdan").

%% API
-export([duplicate/1,duplicate2/1]).


%% --------------------------
%% implementation of task p14
%% --------------------------
duplicate([]) -> [];
duplicate([H|T]) -> [H, H] ++ duplicate(T).


%% --------------------------
%% implementation of task p14
%% with tail recursion
%% --------------------------
duplicate2([])-> [];
duplicate2(List) -> dup(List, []).
dup([], Res) -> p05:reverse(Res);
dup([H|T], Res) -> dup(T, [H,H|Res]).
