%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. трав 2015 21:18
%%%-------------------------------------------------------------------
-module(p05).
-author("Bohdan").

%% API
-export([reverse/1,reverse2/1]).


%% ---------------------------
%% implementation of task p05
%% with tail rec
%% ---------------------------
reverse(List) -> reverse(List, []).
reverse([], ReversList) -> ReversList;
reverse([H|T], ReversList)-> reverse(T, [H|ReversList]).


%% simple solve
reverse2([H|T]) -> reverse2(T) ++ [H];
reverse2([]) -> [].