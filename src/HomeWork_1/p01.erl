%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. трав 2015 20:01
%%%-------------------------------------------------------------------
-module(p01).
-author("Bohdan").

%% API
-export([last/1]).


%% --------------------------
%% implementation of task PO1
%%---------------------------
last([]) -> "Error: empty list";
last([H|[]]) -> H;
last([_|T]) -> last(T).
