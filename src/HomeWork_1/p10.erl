%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. трав 2015 17:45
%%%-------------------------------------------------------------------
-module(p10).
-author("Bohdan").

%% API
-export([encode/1]).


%% --------------------------
%% implementation of task p10
%% --------------------------
encode([]) -> [];
encode(List) ->
  Target = p09:pack(List),
  lenOfDup(Target, []).

lenOfDup([], Res) -> p05:reverse(Res);
lenOfDup([[H|T1]|T2], Res) ->
  Length = p04:len([H|T1]),
  lenOfDup(T2, [{Length, H}|Res]).
