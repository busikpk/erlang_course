%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. june 2015 10:26
%%%-------------------------------------------------------------------
-module(table_test).
-author("Bohdan").

-include_lib("eunit/include/eunit.hrl").

%% ------------------------------------
%% test function
%% ------------------------------------
table_test_() ->
   [
    %% create table and put into the table some values
    ?_assertEqual(true,table:fill_table()),

    %% get row when id = 1
    ?_assertEqual({1,"Marik"},table:get(1)),

    %% get row with id = 111111 this id not contains in the table
    ?_assertEqual([], table:get(111111)),

    %% delete test
    ?_assertEqual(true, table:delete(1)),
    ?_assertEqual([], table:get(1)),

    %% subtable test
    ?_assertEqual([{2, "Selik"},
                   {3, "Korochkin"}],
                   table:subtable({{2001,01,01},{01,01,01}}
                                ,{{2002,01,01},{01,01,01}})),

     %% testing KeepTime set and get
     ?_assert(table:setKeepTime(2,22222222222222)),
     ?_assertEqual(22222222222222,table:getKeepTime(2)),

     %% put test
     ?_assert(table:put(5,"Fggdfg")),
     ?_assertEqual({5,"Fggdfg"}, table:get(5))
   ].
