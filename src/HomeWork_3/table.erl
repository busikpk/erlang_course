%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. june 2015 14:04
%%%-------------------------------------------------------------------
-module(table).
-author("Bohdan").
-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([init/0, put/2, get/1, fill_table/0, delete/1, get_table/0,
         subtable/2, fast_subtable/2, getCreateTime/1, getKeepTime/1,
         finalize/0, setKeepTime/2, next/1, first/0]).


%% -----------------------------------
%% init container
%% -----------------------------------
init() ->
  ets:new(table,[ordered_set, public, named_table]).


%% ------------------------------------
%% insert to table (Create and Update)
%% @param Key - id
%% @param Value - data
%% @param KeepTime - how long table contains row by Key
%% ------------------------------------
put(Key, Value) ->
  Time = erlang:localtime(),
  ets:insert(table,{Key, Value, Time, null}).


%% -------------------------------------
%% read from the table
%% @param Key - id of the target element
%% -------------------------------------
get(Key) ->
  try
    [{Key,Value,_Time,_KeepTime}] = ets:lookup(table, Key),
    {Key,Value}
  catch
     error :_ -> []
  end .


%% -------------------------------------
%% return first element
%% -------------------------------------
first() -> ets:first(table).


%% -------------------------------------
%% return next element after elemnt by Key
%% -------------------------------------
next(Key) -> ets:next(table,Key).


%% -------------------------------------
%% function return how long table must
%% contains row by Key
%% @param Key - row id
%% -------------------------------------
getKeepTime(Key) ->
  try
    [{_Key, _Value, _Time, KeepTime}] = ets:lookup(table, Key),
    KeepTime
  catch
      error :_ -> -1
  end.



%% -------------------------------------
%% function set KeepTime for row in the table
%% @param KeepTime - how long table contains row by Key
%% -------------------------------------
setKeepTime(Key, KeepTime) ->
  try
    [{Key, Value, Time, _OldKeepTime}] = ets:lookup(table, Key),
    ets:insert(table, {Key, Value, Time, KeepTime})
  catch
     error :_ -> table_hasnt_this_key
  end .


%% -------------------------------------
%% function return when row was created or updated
%% @param Key - row id
%% -------------------------------------
getCreateTime(Key) ->
  try
    [{_Key, _Vlaue, Time, _KeepTime}] = ets:lookup(table, Key),
    Time
  catch
      error :_ -> -1
  end.



%% -------------------------------------
%% delete from table
%% @param Key - id of the target element
%% -------------------------------------
delete(Key) ->
  ets:delete(table, Key).


%% ------------------------------------
%% function return subtable with items
%% between Left and Right bounds
%% @param Left - min posible date in subtable
%% @param Right - max posible date in subtable
%% ------------------------------------
subtable(Left, Right) ->
  Pattern = ets:fun2ms(
    fun({Key, Value, Time, _KeepTime}) when Time >= Left, Time =< Right -> {Key, Value} end),
  ets:select(table, Pattern).


%% -------------------------------------------
%% get subtable with using next() and first()
%% -------------------------------------------
fast_subtable(Left, Right) -> fast_subtable(Left, Right, ets:first(table), []).
fast_subtable(_, _, '$end_of_table', Ans) -> p05:reverse(Ans);
fast_subtable(Left, Right, Key, Ans) ->
  [{Key, Value, Time, _KeepTime}] = ets:lookup(table, Key),
  if
    Time >= Left andalso Right >= Time ->
        fast_subtable(Left, Right, ets:next(table, Key), [{Key, Value}|Ans]);
    true ->
        fast_subtable(Left, Right, ets:next(table, Key), Ans)
  end.


%% ------------------------------------------
%% delete table
%% ------------------------------------------
finalize() ->
  ets:delete(table).


%% for testing
%%-------------------------------------------

%% test interval {{2001,01,01},{01,01,01}},{{2002,01,01},{01,01,01}}

%% @param Time - client set time manually
put(Key, Value, Time, KeepTime) ->
  ets:insert(table, {Key, Value, Time, KeepTime}).

get_table() ->
  ets:tab2list(table).

fill_table()->
  init(),
  put(1, "Marik",{{2001,05,21},{01,01,01}},10),
  put(2, "Selik",{{2001,01,01},{11,01,01}},20),
  put(3, "Korochkin",{{2002,01,01},{01,01,01}},30),
  put(4, "Simon",{{2004,01,01},{01,01,01}},40).




