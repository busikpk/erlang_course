%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 02. черв 2015 12:36
%%%-------------------------------------------------------------------
-module(bs01).
-author("Bohdan").

%% API
-export([first_word/1]).

%% -------------------------------------------
%% implementation of task bs01
%% -------------------------------------------
first_word(Str) -> search(Str, <<>>).
search(<<" ",_/binary>>, Res)-> Res;
search(<<X, Rest/binary>>, Res) -> search(Rest, <<Res/binary, X>>);
search(<<>>, Res)-> Res.
