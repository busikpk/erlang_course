%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 03. черв 2015 12:04
%%%-------------------------------------------------------------------
-module(bs04).
-author("Bohdan").

%% API
-export([decode_xml/1]).

%% -------------------------------------
%% implementation of task bs04 ver. 4.0
%% -------------------------------------
decode_xml(BinStr) ->
  decoder(BinStr, [], <<>>).

decoder(<<"</", _/binary>>, [Root], _) ->
  reverse(Root);

decoder(<<"</", Rest/binary>>, [CurrentTag|T], Text) ->
  {_, NewRest} = make_tag(Rest),
  NewStack = head_add(T, reverse(tag_add(CurrentTag, Text))),
  decoder(NewRest, NewStack, <<>>);

decoder(<<"<", Rest/binary>>, Stack, Text) ->
  {TagName, NewRest} = make_tag(Rest),
  Tmp = head_add(Stack, Text),
  decoder(NewRest, [{TagName,[],[]}|Tmp], <<>>);

decoder(<<X, Rest/binary>>, Stack, Text) ->
  decoder(Rest, Stack, <<Text/binary, X>>).

make_tag(Str) -> make_tag(Str, <<>>).
make_tag(<<">", Rest/binary>>, Tag) -> {Tag, Rest};
make_tag(<<X, Rest/binary>>, Tag) -> make_tag(Rest, <<Tag/binary, X>>).

tag_add(Tag, <<>>) ->  Tag;
tag_add({Tag, _, List}, Target) -> {Tag, [], [Target|List]}.

head_add([H|T], Target) -> [tag_add(H, Target)|T];
head_add(List, _) -> List.

reverse({Tag, _, Container}) -> {Tag, [], p05:reverse(Container)}.


%% Xml = <<"<start><item>Text1</item><item>Text2</item><root><node>eeeee</node></root></start>">>.