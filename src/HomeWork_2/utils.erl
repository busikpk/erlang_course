%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%% Module 'utils' contains basic operations with
%%% binary strings
%%% @end
%%% Created : 02. черв 2015 12:58
%%%-------------------------------------------------------------------
-module(utils).
-author("Bohdan").

%% API
-export([bin_reverse/1, bin_length/1, checker/1,bin_substring/3]).


%% ----------------------------------
%% function for rotate binary string
%% ---------------------------------
bin_reverse(Str)-> bin_reverse(Str, <<>>).
bin_reverse(<<X, Rest/binary>>, Ans) ->
  bin_reverse(<<Rest/binary>>, <<X, Ans/binary>>);
bin_reverse(<<>>, Ans) -> Ans.


%% ----------------------------------
%% get length of binary string
%% ----------------------------------
bin_length(Str) -> bin_length(Str, 0).
bin_length(<<_, Rest/binary>>, Len) -> bin_length(<<Rest/binary>>, Len + 1);
bin_length(<<>>, Len) -> Len.


%% ---------------------------------
%% function delete all empty strings
%% if List contains them
%% ---------------------------------
checker(List) -> checker(List, []).
checker([H|T], Ans) ->
  Len = utils:bin_length(H),
  if
    Len == 0 -> checker(T, Ans);
    true -> checker(T, [H|Ans])
  end;
checker([], Ans) -> p05:reverse(Ans).


%% ---------------------------------
%% get bin_substring
%% ---------------------------------
bin_substring(String, Start, End)->
  substring(String, 1, Start, End, <<>>).
substring(<<>>, _, _, _, Res) -> Res;
substring(<<X,Rest/binary>>, Current, Start, End, Res) ->
  if
    Current == End -> bin_reverse(<<X,Res/binary>>);
    Current >= Start -> substring(<<Rest/binary>>, Current + 1, Start, End, <<X,Res/binary>>) ;
    true -> substring(<<Rest/binary>>, Current + 1, Start, End, Res)
  end.