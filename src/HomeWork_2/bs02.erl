%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 02. черв 2015 12:45
%%%-------------------------------------------------------------------
-module(bs02).
-author("Bohdan").

%% API
-export([words/1]).


%% ----------------------------------
%% implementation of task bs02
%% @see utils
%% ---------------------------------
words(Str) -> words(Str, <<>>, []).

words(<<" ", Rest/binary>>, Word, Ans) ->
  words(<<Rest/binary>>, <<>>, [Word|Ans]);

words(<<X, Rest/binary>>, Word, Ans) ->
  words(<<Rest/binary>>, <<Word/binary,X>>, Ans);

%% checker search empty binstrings and remove them
words(<<>>, Last , Ans) -> p05:reverse(utils:checker([Last|Ans])).



