%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 02. черв 2015 13:25
%%%-------------------------------------------------------------------
-module(bs03).
-author("Bohdan").

%% API
-export([split2/2, split/2]).


%% -----------------------------------
%% implementation of task bs03 ver 2.0
%% ------------------------------------
split(Str, SplitValue) ->
  if
    is_binary(SplitValue) ->
        split(Str, byte_size(SplitValue), SplitValue, <<>>, []);
    true ->
        Tmp = list_to_binary(SplitValue),
        split(Str, byte_size(SplitValue), Tmp, <<>>, [])
  end.

split(Str, SplitSize, Split, Word, Words) ->
  case Str of
    <<Split:SplitSize/binary, Rest/binary>> ->
      split(Rest, SplitSize, Split, <<>>, [Word|Words]);
    <<B:1/binary, Rest/binary>> ->
      split(Rest, SplitSize, Split, <<Word/binary, B:1/binary>>, Words);
    _ ->
      p05:reverse([Word | Words])
  end.


%% ------------------------------------
%% implementation of task bs03 ver 1.0
%% hard way)
%% @see utils
%% ------------------------------------
split2(Str, SplitValue) ->
    BinSplitVal = list_to_binary(SplitValue),
    Len = utils:bin_length(BinSplitVal),
    OriginLen = utils:bin_length(Str),
    List = searchIndexes(Str, BinSplitVal),
  if
    List == [] -> Str;
    true ->
       Positions = [X - Len || X <- List ],
       Pairs = make_pairs(Positions, 0 - Len ,Len, OriginLen, []),
       spliter(Str, Pairs, [])
  end.


spliter(_, [], Res) -> p05:reverse(Res);
spliter(Str, [{Start,End}|T], Res) ->
  if
    Start > End -> spliter(Str, T, Res);
    true ->
      S = utils:bin_substring(Str, Start, End),
      spliter(Str, T, [S|Res])
  end.


%% pair from list of pairs contains start index
%% and last index of substring in main string
make_pairs([], Prev, Len, OLen, Res) -> p05:reverse([{Prev + Len + 1 , OLen}|Res]);
make_pairs([End|T], Prev, Len, Olen, Res) ->
  make_pairs(T, End, Len, Olen, [{Prev + Len + 1 , End}|Res]).

%% search where in main string
%% lay substrings
searchIndexes(String, Sub) ->
  LenSub = utils:bin_length(Sub),
  LenStr = utils:bin_length(String),
  if
    LenSub > LenStr -> [];
    true -> searchIndexes(String, Sub, Sub, 0,[])
  end.

searchIndexes(<<>>, _, _, _, Indexes) ->
  p05:reverse(Indexes);

searchIndexes(String, <<>>, Sub, Acc, Indexes) ->
  searchIndexes(String, Sub, Sub, Acc ,[Acc|Indexes]);

searchIndexes(<<X, MainTail/binary>>, <<X, SubTail/binary>>, Sub, Acc, Indexes) ->
  searchIndexes(<<MainTail/binary>>, <<SubTail/binary>>, Sub, Acc + 1, Indexes);

searchIndexes(<<_X, MainTail/binary>>, <<_Y, _SubTail/binary>>, Sub, Acc, Indexes)->
  searchIndexes(<<MainTail/binary>>, Sub, Sub, Acc + 1, Indexes).

