-module(handler).
-behaviour(cowboy_http_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

%%-record(state, {}).

init(_, Req, Opts) ->
        Req2 = cowboy_req:reply(200, [{<<"content-type">>, <<"text/plain">>}], <<"Hello server">>, Req),
        {ok, Req2, Opts}.
	%%{ok, Req, #state{}}.

writer(File,Content) ->
	file:write_file(File, io_lib:fwrite("~p.\n", [Content])).

response(Req, State, Status, Body) ->
        writer("ok.txt","response ok"),
	Json = jsx:encode(Body),
	writer("out.txt",Json),
	{ok, Req2} = cowboy_req:reply(Status,[{<<"content-type">>,<<"application/json">>}], Json, Req),
	{ok, Req2, State}.


handle(Req, State) ->
	{ok, Body, _} = cowboy_req:body(Req),
        writer("request.txt",Body),
	case jsx:decode(Body) of
		[{<<"action">>,<<"insert">>},{<<"key">>,Key},{<<"value">>,Value}] ->
				cache_server:insert(Key,Value),
			  response(Req, State, 200, [{<<"ok">>, <<"ok">>}]);

		[{<<"action">>,<<"lookup">>},{<<"key">>,Key}] ->
			  %% todo checking valid key
			  {Key,Value} = cache_server:get(Key),
				response(Req, State, 200, [{<<"key">>,Key}, {<<"value">>,Value}]);

		[{<<"action">>,<<"lookup_by_date">>},{<<"date_from">>,From},{<<"date_to">>,To}] ->
				Res = cache_server:lookup_by_date(get_date(From), get_date(To)),
			  response(Req, State, 200, [{<<"subtable">>, Res}])
	end.

terminate(_Reason, _Req, _State) ->
	ok.

get_date(Bin) ->
	[Date, Time] = binary:split(Bin, <<" ">>),
	[Year, Month, Day] = binary:split(Date, <<"/">>, [global]),
	[Hour, Minute, Second] = binary:split(Time, <<":">>, [global]),
	{{binary_to_integer(Year), binary_to_integer(Month), binary_to_integer(Day)},
		{binary_to_integer(Hour), binary_to_integer(Minute), binary_to_integer(Second)}}.
