%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%% I use module "table" from HomeWork_3
%%% @end
%%% Created : 11. черв 2015 11:14
%%%-------------------------------------------------------------------
-module(cache_server).
-author("Bohdan").

-behaviour(gen_server).
-define(SERVER, ?MODULE).

%% API
-export([start_link/0]).
-export([init/1,handle_call/3,handle_cast/2,handle_info/2,terminate/2,code_change/3]).
-export([start_link/1,insert/2,setKeepTime/2,lookup_by_date/2,delete/1, get/1,cache_cleaner/1]).


-record(config, {max_time}).

%% ----------------------------------------------------
%%                      Server API
%% ----------------------------------------------------
start_link({Time}) ->
  table:fill_table(),
  gen_server:start_link({global, ?SERVER}, ?MODULE, [Time], []).


insert(Key, Value) ->
  gen_server:call({global, ?MODULE}, {insert,Key,Value}) .


setKeepTime(Key, Time) ->
  table:setKeepTime(Key, Time).


get(Key) ->
  CurrentTime = calendar:datetime_to_gregorian_seconds(erlang:localtime()),
  ItemKeepTime = table:getKeepTime(Key),
  ItemAddedTime = table:getCreateTime(Key),
  if
    ItemAddedTime =:= -1 orelse ItemKeepTime =:= -1 -> {} ;
    true ->
      case CurrentTime - ItemAddedTime > ItemKeepTime of
           true -> table:get(Key);
           false -> {}
      end
  end.


delete(Key) ->
  table:delete(Key).


lookup_by_date(DateFrom, DateTo) ->
  table:fast_subtable(DateFrom, DateTo).


%------------------------------------------------------
%% gen_server functions
%------------------------------------------------------
start_link() ->
  gen_server:start_link({global, ?SERVER}, ?MODULE, [], []).


init([Time]) ->
  io:format("~p~n",[Time]),
  self() ! check,
  {ok, #config{max_time = Time}}.


%% ---------------------------------------------
%%  calls
%% ---------------------------------------------
handle_call({insert,Key, Value}, _From, State) ->
  table:put(Key, Value),
  io:format("~p~n",[State#config.max_time]),
  table:setKeepTime(Key, State#config.max_time),
  {reply, {ok,table:get(Key)}, State}.

%% ---------------------------------------------
%%                    cast
%% ---------------------------------------------
handle_cast(_Request, State) ->
  {noreply, State}.


%% --------------------------------------------
%%                 other hendler
%% --------------------------------------------
handle_info(check, State) ->
  new_scan_thread(),
  %%io:format("loop~n"),
  timer:sleep(1000),
  self() ! check,
  {noreply,State};

handle_info(stop, State) ->
  {noreply, State}.


%% --------------------------------------------
%% close table and server
%% --------------------------------------------
terminate(_Reason, _State) -> table:finalize(), ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


%% --------------------------------------------
%% function for scanning table
%% --------------------------------------------
new_scan_thread()->
  CurrentTime = calendar:datetime_to_gregorian_seconds(erlang:localtime()),
  spawn(?MODULE, cache_cleaner, [CurrentTime]).


cache_cleaner(CurrentTime) ->
  cache_cleaner(CurrentTime, table:first()).

cache_cleaner(_CurrTime, '$end_of_table') -> ok;

cache_cleaner(CurrentTime, Key) ->
  KeepTime = table:getKeepTime(Key),
  CreatTime = calendar:datetime_to_gregorian_seconds(table:getCreateTime(Key)),
  case KeepTime + CreatTime < CurrentTime  of
      true ->
             table:delete(Key),
             io:format("Element by ~p key was deleted",[Key]),
             cache_cleaner(CurrentTime, table:next(Key));
      _ -> cache_cleaner(CurrentTime, table:next(Key))
end.



